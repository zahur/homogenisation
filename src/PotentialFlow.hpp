/*
 * This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

// ThermalElement

#ifndef __POTENTIAL_FLOW_HPP__
#define __POTENTIAL_FLOW_HPP__

struct PotentialFlow {

  struct VolumeFE : public MoFEM::VolumeElementForcesAndSourcesCore {
    VolumeFE(MoFEM::Interface &m_field)
        : MoFEM::VolumeElementForcesAndSourcesCore(m_field) {}
    int getRule(int order) { return 2 * (order - 1); };
  };
  VolumeFE feLhs; //< calculate left hand side for tetrahedral elements
  VolumeFE &getLoopFeLhs() { return feLhs; } ///< get lhs volume element

  struct TriFE : public MoFEM::FaceElementForcesAndSourcesCore {
    TriFE(MoFEM::Interface &m_field)
        : MoFEM::FaceElementForcesAndSourcesCore(m_field) {}
    int getRule(int order) { return 2 * order; };
  };
  TriFE fePressure;                                 //< heat flux element
  TriFE &getLoopFePressure() { return fePressure; } //< get heat flux element

  MoFEM::Interface &mField;
  PotentialFlow(MoFEM::Interface &m_field)
      : feLhs(m_field), fePressure(m_field), mField(m_field) {}

  struct PressureData {
    PressureCubitBcData dAta;
    Range tRis;
  };
  std::map<int, PressureData>
      setOfPressures; ///< maps side set id with appropriate Pressure Data

  // in case of single fibre tets are added here
  MoFEMErrorCode addPotentialFlowElements(
      const std::string field_name,
      const std::string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
    MoFEMFunctionBegin;

    CHKERR mField.add_finite_element("POTENTIALFLOW_FE", MF_ZERO);
    CHKERR mField.modify_finite_element_add_field_row("POTENTIALFLOW_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_col("POTENTIALFLOW_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_data("POTENTIALFLOW_FE",
                                                       field_name);
    if (mField.check_field(mesh_nodals_positions)) {
      CHKERR mField.modify_finite_element_add_field_data("POTENTIALFLOW_FE",
                                                         mesh_nodals_positions);
    }

    for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
             mField, BLOCKSET | UNKNOWNNAME, it)) {

      if (it->getName() == "PotentialFlow_1") {
        Range tets_in_block;
        CHKERR mField.get_moab().get_entities_by_type(it->meshset, MBTET,
                                                      tets_in_block, true);
        CHKERR mField.add_ents_to_finite_element_by_type(tets_in_block, MBTET,
                                                         "POTENTIALFLOW_FE");
      }
    }
    MoFEMFunctionReturn(0);
  }

  // in case of multi-fibres no tets are added here
  //test will be added in main in a loop
  MoFEMErrorCode addPotentialFlowElementsMultiFibres(
      const std::string field_name,
      const std::string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
    MoFEMFunctionBegin;

    CHKERR mField.add_finite_element("POTENTIALFLOW_FE", MF_ZERO);
    CHKERR mField.modify_finite_element_add_field_row("POTENTIALFLOW_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_col("POTENTIALFLOW_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_data("POTENTIALFLOW_FE",
                                                       field_name);
    if (mField.check_field(mesh_nodals_positions)) {
      CHKERR mField.modify_finite_element_add_field_data("POTENTIALFLOW_FE",
                                                         mesh_nodals_positions);
    }
      MoFEMFunctionReturn(0);
  }


  // in case of single fibre tris are added here
  MoFEMErrorCode addPressureElement(
      const std::string field_name,
      const std::string mesh_nodals_positions = "MESH_NODE_POSITIONS") {

    MoFEMFunctionBegin;

    CHKERR mField.add_finite_element("PRESSURE_FE", MF_ZERO);
    CHKERR mField.modify_finite_element_add_field_row("PRESSURE_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_col("PRESSURE_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_data("PRESSURE_FE",
                                                       field_name);
    if (mField.check_field(mesh_nodals_positions)) {
      CHKERR mField.modify_finite_element_add_field_data("PRESSURE_FE",
                                                         mesh_nodals_positions);
    }

    for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
             mField, SIDESET | PRESSURESET, it)) {
      CHKERR it->getBcDataStructure(setOfPressures[it->getMeshsetId()].dAta);
      CHKERR mField.get_moab().get_entities_by_type(
          it->meshset, MBTRI, setOfPressures[it->getMeshsetId()].tRis, true);
      CHKERR mField.add_ents_to_finite_element_by_type(
          setOfPressures[it->getMeshsetId()].tRis, MBTRI, "PRESSURE_FE");
    }
    MoFEMFunctionReturn(0);
  }

  // in case of multi-fibres no tris are added here
  // tris will be added in main in a loop
  MoFEMErrorCode addPressureElementMultiFibres(
      const std::string field_name,
      const std::string mesh_nodals_positions = "MESH_NODE_POSITIONS") {

    MoFEMFunctionBegin;

    CHKERR mField.add_finite_element("PRESSURE_FE", MF_ZERO);
    CHKERR mField.modify_finite_element_add_field_row("PRESSURE_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_col("PRESSURE_FE",
                                                      field_name);
    CHKERR mField.modify_finite_element_add_field_data("PRESSURE_FE",
                                                       field_name);
    if (mField.check_field(mesh_nodals_positions)) {
      CHKERR mField.modify_finite_element_add_field_data("PRESSURE_FE",
                                                         mesh_nodals_positions);
    }
    MoFEMFunctionReturn(0);
  }

  MoFEMErrorCode setPotentialFlowElementLhsOperators(string field_name, Mat A) {
    MoFEMFunctionBegin;
    feLhs.getOpPtrVector().push_back(new OpPotentialFlowLhs(field_name, A));
    MoFEMFunctionReturn(0);
  }

  struct OpPotentialFlowLhs
      : public MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator {
    Mat A;
    OpPotentialFlowLhs(const std::string field_name, Mat _A)
        : MoFEM::VolumeElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPROWCOL),
          A(_A) {
      sYmm = true;
    }

    MatrixDouble K, transK;
    MoFEMErrorCode doWork(int row_side, int col_side, EntityType row_type,
                          EntityType col_type,
                          DataForcesAndSourcesCore::EntData &row_data,
                          DataForcesAndSourcesCore::EntData &col_data) {
      MoFEMFunctionBegin;

      if (row_data.getIndices().size() == 0)
        MoFEMFunctionReturnHot(0);
      if (col_data.getIndices().size() == 0)
        MoFEMFunctionReturnHot(0);

      int nb_row = row_data.getN().size2();
      int nb_col = col_data.getN().size2();
      K.resize(nb_row, nb_col);
      K.clear();
      for (unsigned int gg = 0; gg < row_data.getN().size1(); gg++) {

        double val = getVolume() * getGaussPts()(3, gg);
        if (getHoGaussPtsDetJac().size() > 0) {
          val *= getHoGaussPtsDetJac()[gg]; ///< higher order geometry
        }

        // ublas
        MatrixDouble K1 = val * row_data.getDiffN(gg, nb_row);
        noalias(K) += prod(K1, trans(col_data.getDiffN(gg, nb_col)));
      }

      // cout << "row_data.getIndices().size() " << row_data.getIndices().size()
      // << endl;
      // cout << "col_data.getIndices().size() " << col_data.getIndices().size()
      //  << endl;

      // cout << "K  "<<K << endl;
      // cout << "Hi 1" << endl;
      // cout << "row_type "<< row_type << endl;
      // cout << "row_side " << row_side << endl;

      // cout << "col_type " << col_type << endl;
      // cout << "col_side " << col_side << endl;

      // cout << "row indices " << endl;
      // for (int ii = 0; ii < row_data.getIndices().size(); ii++) {
      //   cout << row_data.getIndices()[ii] << endl;
      // }

      // cout << "col indices " << endl;
      // for (int ii = 0; ii < col_data.getIndices().size(); ii++) {
      //   cout << col_data.getIndices()[ii] << endl;
      // }
      // // cout << "MatSetValues Hi 1" << endl;
      // CHKERR MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
      // CHKERR MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
      // MatView(A, PETSC_VIEWER_STDOUT_WORLD);
      

      CHKERR MatSetValues(A, nb_row, &row_data.getIndices()[0], nb_col,
                          &col_data.getIndices()[0], &K(0, 0), ADD_VALUES);

      // cout << "MatSetValues Hi 2" << endl;

      if (row_side != col_side || row_type != col_type) {
        transK.resize(nb_col, nb_row);
        noalias(transK) = trans(K);
        CHKERR MatSetValues(A, nb_col, &col_data.getIndices()[0], nb_row,
                            &row_data.getIndices()[0], &transK(0, 0),
                            ADD_VALUES);
      }
      // cout << "Hi 2" << endl;

      // string aaa;
      // cin >> aaa;
      MoFEMFunctionReturn(0);
    }
  };

  MoFEMErrorCode setPotentialFlowPressureElementRhsOperators(
      string field_name, Vec F,
      const std::string mesh_nodals_positions = "MESH_NODE_POSITIONS") {
    MoFEMFunctionBegin;
    bool hoGeometry = false;
    if (mField.check_field(mesh_nodals_positions)) {
      hoGeometry = true;
    }
    std::map<int, PressureData>::iterator sit = setOfPressures.begin();
    for (; sit != setOfPressures.end(); sit++) {
      // add finite element
      fePressure.getOpPtrVector().push_back(
          new OpPotentialPressure(field_name, F, sit->second, hoGeometry));
    }
    MoFEMFunctionReturn(0);
  }

  struct OpPotentialPressure
      : public MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator {
    PressureData &dAta;
    bool hoGeometry;
    Vec F;
    OpPotentialPressure(const std::string field_name, Vec _F,
                        PressureData &data, bool ho_geometry = false)
        : MoFEM::FaceElementForcesAndSourcesCore::UserDataOperator(
              field_name, UserDataOperator::OPROW),
          dAta(data), hoGeometry(ho_geometry), F(_F) {}

    VectorDouble Nf;

    MoFEMErrorCode doWork(int side, EntityType type,
                          DataForcesAndSourcesCore::EntData &data) {
      MoFEMFunctionBegin;
      if (data.getIndices().size() == 0)
        MoFEMFunctionReturnHot(0);
      if (dAta.tRis.find(getNumeredEntFiniteElementPtr()->getEnt()) ==
          dAta.tRis.end())
        MoFEMFunctionReturnHot(0);

      const auto &dof_ptr = data.getFieldDofs()[0];
      int rank = dof_ptr->getNbOfCoeffs();

      int nb_dofs = data.getIndices().size() / rank;

      Nf.resize(data.getIndices().size());
      Nf.clear();

      for (unsigned int gg = 0; gg < data.getN().size1(); gg++) {
        double val = getGaussPts()(2, gg);
        double pressure;

        if (hoGeometry) {
          const double area = norm_2(getNormalsAtGaussPts(gg)) * 0.5;
          // cout << "dAta.dAta.data.value1  = " << dAta.dAta.data.value1  <<
          // endl;
          pressure = dAta.dAta.data.value1 * area;
        } else {
          pressure = dAta.dAta.data.value1 * getArea();
        }
        ublas::noalias(Nf) += val * pressure * data.getN(gg, nb_dofs);
      }

      CHKERR VecSetValues(F, data.getIndices().size(), &data.getIndices()[0],
                          &Nf[0], ADD_VALUES);
      MoFEMFunctionReturn(0);
    }
  };
};

#endif //__POTENTIAL_FLOW_HPP__