/** \file potential_flow.cpp
  \ingroup mofem_homogenisation
  \brief Example fibre direction using potential flow
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <PotentialFlow.hpp>

using namespace MoFEM;

namespace bio = boost::iostreams;
using bio::stream;
using bio::tee_device;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  const string default_options = "-ksp_type fgmres \n"
                                 "-pc_type lu \n"
                                 "-pc_factor_mat_solver_package mumps \n"
                                 "-ksp_monitor \n";

  string param_file = "param_file.petsc";
  if (!static_cast<bool>(ifstream(param_file))) {
    std::ofstream file(param_file.c_str(), std::ios::ate);
    if (file.is_open()) {
      file << default_options;
      file.close();
    }
  }

  MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);

  try {

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    PetscBool flg = PETSC_TRUE;
    char mesh_file_name[255];
    CHKERR PetscOptionsGetString(PETSC_NULL, PETSC_NULL, "-my_file",
                                 mesh_file_name, 255, &flg);
    if (flg != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_FOUND,
              "*** ERROR -my_file (MESH FILE NEEDED)");
    }

    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

    const char *option;
    option = ""; //"PARALLEL=BCAST;";//;DEBUG_IO";
    CHKERR moab.load_file(mesh_file_name, 0, option);

    // Create MoFEM (Joseph) database
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    // set entities bit level
    BitRefLevel bit_level0;
    bit_level0.set(0);
    EntityHandle meshset_level0;
    CHKERR moab.create_meshset(MESHSET_SET, meshset_level0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);

    // Fields
    CHKERR m_field.add_field("POTENTIAL_FIELD", H1, AINSWORTH_LEGENDRE_BASE, 1);

    // Problem
    CHKERR m_field.add_problem("POTENTIALFLOW_PROBLEM");

    // set refinement level for problem
    CHKERR m_field.modify_problem_ref_level_add_bit("POTENTIALFLOW_PROBLEM",
                                                    bit_level0);

    // meshset consisting all entities in mesh
    EntityHandle root_set = moab.get_root_set();
    // add entities to field
    CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET, "POTENTIAL_FIELD");

    // set app. order
    // see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes
    // (Mark Ainsworth & Joe Coyle)
    PetscInt order;
    CHKERR PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-my_order", &order,
                              &flg);
    if (flg != PETSC_TRUE) {
      order = 2;
    }

    CHKERR m_field.set_field_order(root_set, MBTET, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBTRI, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBEDGE, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBVERTEX, "POTENTIAL_FIELD", 1);

    CHKERR m_field.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                             3);
    CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET,
                                             "MESH_NODE_POSITIONS");
    CHKERR m_field.set_field_order(0, MBTET, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBTRI, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBEDGE, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);

   
    PotentialFlow potential_flow(m_field);
    CHKERR potential_flow.addPotentialFlowElements("POTENTIAL_FIELD");
    CHKERR potential_flow.addPressureElement("POTENTIAL_FIELD");

    CHKERR m_field.modify_problem_add_finite_element("POTENTIALFLOW_PROBLEM",
                                                     "POTENTIALFLOW_FE");
    CHKERR m_field.modify_problem_add_finite_element("POTENTIALFLOW_PROBLEM",
                                                     "PRESSURE_FE");
    // build field
    CHKERR m_field.build_fields();
    // build finite elemnts
    CHKERR m_field.build_finite_elements();
    // build adjacencies
    CHKERR m_field.build_adjacencies(bit_level0);


    // get HO geometry for 10 node tets
    // This method takes coordinates form edges mid nodes in 10 node tet and
    // project values on 2nd order hierarchical basis used to approx. geometry.
   
    Projection10NodeCoordsOnField ent_method_material(m_field,
                                                      "MESH_NODE_POSITIONS");
    CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method_material);

    CHKERR DMRegister_MoFEM("POTENTIALFLOW_PROBLEM");
    auto dm = createSmartDM(m_field.get_comm(), "POTENTIALFLOW_PROBLEM");
    CHKERR DMMoFEMSetIsPartitioned(dm, PETSC_FALSE);

    CHKERR DMMoFEMCreateMoFEM(dm, &m_field, "POTENTIALFLOW_PROBLEM",
                              BitRefLevel().set(0));
    CHKERR DMSetFromOptions(dm);
    // add elements to dM
    CHKERR DMMoFEMAddElement(dm, "POTENTIALFLOW_FE");
    CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
    CHKERR DMSetUp(dm);

    // remove essential DOFs
    Range fixed_vertex;
    for(_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(m_field,NODESET|UNKNOWNNAME,it)) {
      if (it->getName()=="FixedVertex_1"){
          rval = moab.get_entities_by_type(it->meshset, 
                                                MBVERTEX,fixed_vertex,true); 
      }
    }

    /// TODO: Update to lukasz/develop branch and this will work. Then you will not have to use fix_dof_fe 
    // CHKERR m_field.getInterface<ProblemsManager>()
    //     ->removeDofsOnEntitiesNotDistributed("POTENTIALFLOW_PROBLEM", "POTENTIAL_FIELD",
    //                                          fixed_vertex);

    // create matrices and vectors
    auto D = smartCreateDMVector(dm);
    auto F = smartVectorDuplicate(D);
    auto A = smartCreateDMMatrix(dm);

    CHKERR MatZeroEntries(A);
    CHKERR VecZeroEntries(F);

    VecGhostUpdateBegin(F, INSERT_VALUES, SCATTER_FORWARD);
    VecGhostUpdateEnd(F, INSERT_VALUES, SCATTER_FORWARD);

    CHKERR potential_flow.setPotentialFlowElementLhsOperators("POTENTIAL_FIELD", A);
    CHKERR potential_flow.setPotentialFlowPressureElementRhsOperators("POTENTIAL_FIELD", F);

    DirichletFixFieldAtEntitiesBc fix_dof_fe(m_field, "POTENTIAL_FIELD", A, D, F,
                                             fixed_vertex);
    CHKERR DMoFEMPreProcessFiniteElements(dm, &fix_dof_fe);
    CHKERR DMoFEMLoopFiniteElements(dm, "POTENTIALFLOW_FE",
                                    &potential_flow.feLhs);
    CHKERR DMoFEMLoopFiniteElements(dm, "PRESSURE_FE",
                                    &potential_flow.fePressure);
    CHKERR DMoFEMPostProcessFiniteElements(dm, &fix_dof_fe);

    CHKERR MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
    CHKERR MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
    CHKERR VecGhostUpdateBegin(F, ADD_VALUES, SCATTER_REVERSE);
    CHKERR VecGhostUpdateEnd(F, ADD_VALUES, SCATTER_REVERSE);
    CHKERR VecAssemblyBegin(F);
    CHKERR VecAssemblyEnd(F);

    auto solver = createKSP(PETSC_COMM_WORLD);
    CHKERR KSPSetOperators(solver, A, A);
    CHKERR KSPSetFromOptions(solver);
    CHKERR KSPSetUp(solver);
    CHKERR KSPSolve(solver, F, D);

    // VecView(D, PETSC_VIEWER_STDOUT_WORLD);

    CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
    CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

    PostProcVolumeOnRefinedMesh post_proc(m_field);
    CHKERR post_proc.generateReferenceElementMesh();
    CHKERR post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS");
    CHKERR post_proc.addFieldValuesPostProc("POTENTIAL_FIELD");
    CHKERR m_field.loop_finite_elements("POTENTIALFLOW_PROBLEM", 
                  "POTENTIALFLOW_FE",post_proc);
    CHKERR post_proc.writeFile("out_potential_flow.h5m");

  }
  CATCH_ERRORS;

  return MoFEM::Core::Finalize();
}
