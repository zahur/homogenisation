

/** \file potential_flow.cpp
  \ingroup mofem_homogenisation
  \brief Example fibre direction using potential flow
*/

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <PotentialFlow.hpp>

using namespace MoFEM;

namespace bio = boost::iostreams;
using bio::stream;
using bio::tee_device;

static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  const string default_options = "-ksp_type fgmres \n"
                                 "-pc_type lu \n"
                                 "-pc_factor_mat_solver_package mumps \n"
                                 "-ksp_monitor \n";

  string param_file = "param_file.petsc";
  if (!static_cast<bool>(ifstream(param_file))) {
    std::ofstream file(param_file.c_str(), std::ios::ate);
    if (file.is_open()) {
      file << default_options;
      file.close();
    }
  }

  MoFEM::Core::Initialize(&argc, &argv, param_file.c_str(), help);

  try {

    moab::Core mb_instance;
    moab::Interface &moab = mb_instance;
    int rank;
    MPI_Comm_rank(PETSC_COMM_WORLD, &rank);

    PetscBool flg = PETSC_TRUE;
    char mesh_file_name[255];
    CHKERR PetscOptionsGetString(PETSC_NULL, PETSC_NULL, "-my_file",
                                 mesh_file_name, 255, &flg);
    if (flg != PETSC_TRUE) {
      SETERRQ(PETSC_COMM_SELF, MOFEM_NOT_FOUND,
              "*** ERROR -my_file (MESH FILE NEEDED)");
    }
   
    ParallelComm *pcomm = ParallelComm::get_pcomm(&moab, MYPCOMM_INDEX);
    if (pcomm == NULL)
      pcomm = new ParallelComm(&moab, PETSC_COMM_WORLD);

    const char *option;
    option = ""; //"PARALLEL=BCAST;";//;DEBUG_IO";
    CHKERR moab.load_file(mesh_file_name, 0, option);

    // Create MoFEM (Joseph) database
    MoFEM::Core core(moab);
    MoFEM::Interface &m_field = core;

    // set entities bit level
    BitRefLevel bit_level0;
    bit_level0.set(0);
    EntityHandle meshset_level0;
    CHKERR moab.create_meshset(MESHSET_SET, meshset_level0);
    CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(
        0, 3, bit_level0);

    // Fields
    CHKERR m_field.add_field("POTENTIAL_FIELD", H1, AINSWORTH_LEGENDRE_BASE, 1);

    // Problem
    CHKERR m_field.add_problem("POTENTIALFLOW_PROBLEM");

    // set refinement level for problem
    CHKERR m_field.modify_problem_ref_level_add_bit("POTENTIALFLOW_PROBLEM",
                                                    bit_level0);

    // // meshset consisting all entities in mesh
    EntityHandle root_set = moab.get_root_set();
    // // add entities to field
    // meshset consisting all entities in mesh
    CHKERR m_field.add_ents_to_field_by_type(root_set, MBTET, "POTENTIAL_FIELD");

    // set app. order
    // see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes
    // (Mark Ainsworth & Joe Coyle)
    PetscInt order;
    CHKERR PetscOptionsGetInt(PETSC_NULL, PETSC_NULL, "-my_order", &order,
                              &flg);
    if (flg != PETSC_TRUE) {
      order = 2;
    }

    // define field
    CHKERR m_field.set_field_order(root_set, MBTET, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBTRI, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBEDGE, "POTENTIAL_FIELD", order);
    CHKERR m_field.set_field_order(root_set, MBVERTEX, "POTENTIAL_FIELD", 1);

    CHKERR m_field.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                             3);

    // add entities to field
    CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "MESH_NODE_POSITIONS");
    CHKERR m_field.set_field_order(0, MBTET, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBTRI, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBEDGE, "MESH_NODE_POSITIONS", 2);
    CHKERR m_field.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);

    /// Getting No. of Fibres and their index to be used for Potential Flow
    /// Problem
    
    int no_of_fibres = 0;
    for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
             m_field, BLOCKSET | UNKNOWNNAME, it)) {

      std::size_t found = it->getName().find("PotentialFlow");
      if (found == std::string::npos)
        continue;
      no_of_fibres += 1;
    }
    cout << "No. of fibres for potential flow : " << no_of_fibres << endl;



    EntityHandle out_meshset_fibres;
    if (pcomm->rank() == 0) {
      CHKERR moab.create_meshset(MESHSET_SET, out_meshset_fibres);
    }


    // run the problem one fibre at a time
    Range tets_in_block, tris_pressure;
    for (int fibre = 1; fibre <= no_of_fibres; fibre++) {
      // cout << "Fibre no :" << fibre << endl;

      // define elements
      PotentialFlow potential_flow(m_field);
      CHKERR potential_flow.addPotentialFlowElementsMultiFibres("POTENTIAL_FIELD");
      CHKERR potential_flow.addPressureElementMultiFibres("POTENTIAL_FIELD");

      //add elements to the problem
      CHKERR
      m_field.modify_problem_add_finite_element("POTENTIALFLOW_PROBLEM",
                                                       "POTENTIALFLOW_FE");
      CHKERR
      m_field.modify_problem_add_finite_element("POTENTIALFLOW_PROBLEM",
                                                       "PRESSURE_FE");

      tets_in_block.clear();
      ostringstream rr;
      rr << "PotentialFlow_" << fibre;
      for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
               m_field, BLOCKSET | UNKNOWNNAME, it)) {

        if (it->getName() == rr.str().c_str()) {
          CHKERR m_field.get_moab().get_entities_by_type(it->meshset, MBTET,
                                                         tets_in_block, true);
          CHKERR m_field.add_ents_to_finite_element_by_type(
              tets_in_block, MBTET, "POTENTIALFLOW_FE");
        }
      }
      // cout << "tets_in_block " << tets_in_block << endl;

      tris_pressure.clear();
      ostringstream pr1, pr2;
      pr1 << "PressureIO_" << fibre << "_1";
      pr2 << "PressureIO_" << fibre << "_2";

      for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
               m_field, SIDESET|PRESSURESET, it)) {
        if (it->getName() == pr1.str().c_str() ||
            it->getName() == pr2.str().c_str()) {
          CHKERR it->getBcDataStructure(
              potential_flow.setOfPressures[it->getMeshsetId()].dAta);
          CHKERR moab.get_entities_by_type(
              it->meshset, MBTRI,
              potential_flow.setOfPressures[it->getMeshsetId()].tRis, true);
          
          CHKERR m_field.add_ents_to_finite_element_by_type(
              potential_flow.setOfPressures[it->getMeshsetId()].tRis, MBTRI,
              "PRESSURE_FE");
          tris_pressure.merge(
              potential_flow.setOfPressures[it->getMeshsetId()].tRis); 
        }
      }
      // cout << "tris_pressure : " << tris_pressure;

      // build field
      CHKERR m_field.build_fields();
      // build finite elemnts
      CHKERR m_field.build_finite_elements();
      // build adjacencies
      CHKERR m_field.build_adjacencies(bit_level0);

      // get HO geometry for 10 node tets
      // This method takes coordinates form edges mid nodes in 10 node tet and
      // project values on 2nd order hierarchical basis used to approx.
      // geometry.

      Projection10NodeCoordsOnField ent_method_material(m_field,
                                                        "MESH_NODE_POSITIONS");
      CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method_material);

      CHKERR DMRegister_MoFEM("POTENTIALFLOW_PROBLEM");
      auto dm = createSmartDM(m_field.get_comm(), "POTENTIALFLOW_PROBLEM");
      CHKERR DMMoFEMSetIsPartitioned(dm, PETSC_FALSE);

      CHKERR DMMoFEMCreateMoFEM(dm, &m_field, "POTENTIALFLOW_PROBLEM",
                                BitRefLevel().set(0));
      CHKERR DMSetFromOptions(dm);
      // add elements to dM
      CHKERR DMMoFEMAddElement(dm, "POTENTIALFLOW_FE");
      CHKERR DMMoFEMAddElement(dm, "PRESSURE_FE");
      CHKERR DMSetUp(dm);
      
      // remove essential DOFs
      ostringstream vv;
      vv << "FixedVertex_" << fibre;
      Range fixed_vertex;
      fixed_vertex.clear(); 
      for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
                            m_field, NODESET | UNKNOWNNAME, it)) {
        if (it->getName() == vv.str().c_str()) {
          rval = moab.get_entities_by_type(it->meshset, MBVERTEX, fixed_vertex,
                                           true);
        }
      }
      cout << "fixed_vertex " << fixed_vertex << endl;

      // create matrices and vectors
      auto D = smartCreateDMVector(dm);
      auto F = smartVectorDuplicate(D);
      auto A = smartCreateDMMatrix(dm);

      CHKERR MatZeroEntries(A);
      CHKERR VecZeroEntries(F);

      // CHKERR MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
      // CHKERR MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
      // MatView(A, PETSC_VIEWER_STDOUT_WORLD);
      // string aaa;
      // cin >> aaa;

      // VecView(D, PETSC_VIEWER_STDOUT_WORLD);
      VecGhostUpdateBegin(F, INSERT_VALUES, SCATTER_FORWARD);
      VecGhostUpdateEnd(F, INSERT_VALUES, SCATTER_FORWARD);

      CHKERR potential_flow.setPotentialFlowElementLhsOperators("POTENTIAL_FIELD", A);
      CHKERR potential_flow.setPotentialFlowPressureElementRhsOperators("POTENTIAL_FIELD",
                                                                        F);
      
      DirichletFixFieldAtEntitiesBc fix_dof_fe(m_field, "POTENTIAL_FIELD", A, D, F,
                                               fixed_vertex);

      // cout << "Hi 1 " << endl;
      CHKERR DMoFEMPreProcessFiniteElements(dm, &fix_dof_fe);
      // cout << "Hi 1-1 " << endl;

      CHKERR DMoFEMLoopFiniteElements(dm, "POTENTIALFLOW_FE",
                                      &potential_flow.feLhs);
      // cout << "Hi 2 " << endl;
      CHKERR DMoFEMLoopFiniteElements(dm, "PRESSURE_FE",
                                      &potential_flow.fePressure);
      // cout << "Hi 3 " << endl;
      CHKERR DMoFEMPostProcessFiniteElements(dm, &fix_dof_fe);
      // cout << "Hi 4 " << endl;

      CHKERR MatAssemblyBegin(A, MAT_FINAL_ASSEMBLY);
      CHKERR MatAssemblyEnd(A, MAT_FINAL_ASSEMBLY);
      CHKERR VecGhostUpdateBegin(F, ADD_VALUES, SCATTER_REVERSE);
      CHKERR VecGhostUpdateEnd(F, ADD_VALUES, SCATTER_REVERSE);
      CHKERR VecAssemblyBegin(F);
      CHKERR VecAssemblyEnd(F);

      auto solver = createKSP(PETSC_COMM_WORLD);
      CHKERR KSPSetOperators(solver, A, A);
      CHKERR KSPSetFromOptions(solver);
      CHKERR KSPSetUp(solver);
      CHKERR KSPSolve(solver, F, D);

      // VecView(D, PETSC_VIEWER_STDOUT_WORLD);

      CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
      CHKERR DMoFEMMeshToLocalVector(dm, D, INSERT_VALUES, SCATTER_REVERSE);

      if (pcomm->rank() == 0) {
        CHKERR m_field.get_problem_finite_elements_entities(
            "POTENTIALFLOW_PROBLEM", "POTENTIALFLOW_FE", out_meshset_fibres);
      }


      // remove intities from finite elements (to prepare it for next fibres)
      CHKERR m_field.remove_ents_from_finite_element("POTENTIALFLOW_FE",
                                                     tets_in_block);
      CHKERR m_field.remove_ents_from_finite_element("PRESSURE_FE",
                                                     tris_pressure);
      // clear problems (to prepare it for next fibre)
      CHKERR m_field.clear_problems();

      // string wait;
      // cin >> wait;
    }


    Tag phi;
    double def_val = 0;
    CHKERR moab.tag_get_handle("POTENTIAL_FIELD", 1, MB_TYPE_DOUBLE, phi,
                               MB_TAG_CREAT | MB_TAG_SPARSE, &def_val);
    
    for (_IT_GET_DOFS_FIELD_BY_NAME_FOR_LOOP_(m_field, "POTENTIAL_FIELD", dof)) {
      EntityHandle ent = dof->get()->getEnt();
      double val = dof->get()->getFieldData();
      CHKERR moab.tag_set_data(phi, &ent, 1, &val);
    }

    if (pcomm->rank() == 0) {
      CHKERR moab.write_file("solution_RVE.h5m");

      CHKERR moab.write_file("out_potential_flow.vtk", "VTK", "",
                               &out_meshset_fibres, 1);
    }


  }
  CATCH_ERRORS;

  return MoFEM::Core::Finalize();
}



// PostProcVolumeOnRefinedMesh post_proc(m_field);
// CHKERR post_proc.generateReferenceElementMesh();
// CHKERR post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS");
// CHKERR post_proc.addFieldValuesPostProc("PHI");
// CHKERR m_field.loop_finite_elements("POTENTIALFLOW_PROBLEM", "POTENTIALFLOW_FE",
//                                     post_proc);
// CHKERR post_proc.writeFile("out_potential_flow.h5m");
