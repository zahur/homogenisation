/** \file rve_mechanical.cpp
 * \brief Calculates stiffness matrix for elastic RVE.

 Three types of boundary conditions are implemented, i.e.
 HOMOBCDISP, HOMOBCPERIODIC, HOMOBCTRAC, NITSCHE.

 NITSHCE method allow to apply periodic boundary conditions
 to arbitrary convex shape RVE.

 */

/* This file is part of MoFEM.
 * MoFEM is free software: you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the
 * Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * MoFEM is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
 * FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
 * License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with MoFEM. If not, see <http://www.gnu.org/licenses/>. */

#include <BasicFiniteElements.hpp>
#include <MoFEM.hpp>
using namespace MoFEM;
#include <BCs_RVELagrange_Disp.hpp>

#ifndef WITH_ADOL_C
  #error "MoFEM need to be compiled with ADOL-C"
#endif

#include <adolc/adolc.h>
#include <NonLinearElasticElement.hpp>
#include <Hooke.hpp>
#include <SmallTransverselyIsotropic.hpp>


static char help[] = "...\n\n";

int main(int argc, char *argv[]) {

  MoFEM::Core::Initialize(&argc,&argv,(char *)0,help);

  try {

  moab::Core mb_instance;
  moab::Interface& moab = mb_instance;
  int rank;
  MPI_Comm_rank(PETSC_COMM_WORLD,&rank);

  //Reade parameters from line command
  PetscBool flg = PETSC_TRUE;
  char mesh_file_name[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-my_file",mesh_file_name,255,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    SETERRQ(PETSC_COMM_SELF,MOFEM_NOT_FOUND,"*** ERROR -my_file (MESH FILE NEEDED)");
  }

  PetscInt order;
  ierr = PetscOptionsGetInt(PETSC_NULL,"-my_order",&order,&flg); CHKERRQ(ierr);
  if(flg != PETSC_TRUE) {
    order = 1;
  }

  //Read mesh to MOAB
  const char *option;
  option = "";//"PARALLEL=BCAST;";//;DEBUG_IO";
  rval = moab.load_file(mesh_file_name, 0, option); CHKERRQ_MOAB(rval);
  ParallelComm* pcomm = ParallelComm::get_pcomm(&moab,MYPCOMM_INDEX);
  if(pcomm == NULL) pcomm =  new ParallelComm(&moab,PETSC_COMM_WORLD);

  //Create MoFEM (Joseph) database
  MoFEM::Core core(moab);
  MoFEM::Interface& m_field = core;

  // set entities bit level
  BitRefLevel bit_level0;
  bit_level0.set(0);
  EntityHandle meshset_level0;
  CHKERR moab.create_meshset(MESHSET_SET, meshset_level0);
  CHKERR m_field.getInterface<BitRefManager>()->setBitRefLevelByDim(0, 3,
                                                                    bit_level0);

  //Fields
  int field_rank=3;
  CHKERR m_field.add_field("DISPLACEMENT", H1, AINSWORTH_LEGENDRE_BASE,
                           field_rank);
  CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "DISPLACEMENT");
  CHKERR m_field.add_field("MESH_NODE_POSITIONS", H1, AINSWORTH_LEGENDRE_BASE,
                           field_rank, MB_TAG_SPARSE, MF_ZERO);
  CHKERR m_field.add_ents_to_field_by_type(0, MBTET, "MESH_NODE_POSITIONS");

  //add entitities (by tets) to the field
  CHKERR m_field.add_field("LAGRANGE_MUL_DISP", H1, AINSWORTH_LEGENDRE_BASE,
                           field_rank);

  for (_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field, SIDESET, it)) {
    if (it->getMeshsetId() == 103) {
      Range tris;
      CHKERR moab.get_entities_by_type(it->meshset, MBTRI, tris, true);
      CHKERR m_field.add_ents_to_field_by_type(tris, MBTRI, "LAGRANGE_MUL_DISP");
    }
  }

  //set app. order
  //see Hierarchic Finite Element Bases on Unstructured Tetrahedral Meshes (Mark Ainsworth & Joe Coyle)
  CHKERR m_field.set_field_order(0, MBTET, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBTRI, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBEDGE, "DISPLACEMENT", order);
  CHKERR m_field.set_field_order(0, MBVERTEX, "DISPLACEMENT", 1);

  CHKERR m_field.set_field_order(0, MBTET, "MESH_NODE_POSITIONS", 2);
  CHKERR m_field.set_field_order(0, MBTRI, "MESH_NODE_POSITIONS", 2);
  CHKERR m_field.set_field_order(0, MBEDGE, "MESH_NODE_POSITIONS", 2);
  CHKERR m_field.set_field_order(0, MBVERTEX, "MESH_NODE_POSITIONS", 1);

  CHKERR m_field.set_field_order(0, MBTRI, "LAGRANGE_MUL_DISP", order);
  CHKERR m_field.set_field_order(0, MBEDGE, "LAGRANGE_MUL_DISP", order);
  CHKERR m_field.set_field_order(0, MBVERTEX, "LAGRANGE_MUL_DISP", 1);

   //build field
  CHKERR m_field.build_fields();
  Projection10NodeCoordsOnField ent_method_material(m_field,
                                      "MESH_NODE_POSITIONS");
  CHKERR m_field.loop_dofs("MESH_NODE_POSITIONS", ent_method_material);

  boost::shared_ptr<Hooke<adouble> > hooke_adouble(new Hooke<adouble>());
  boost::shared_ptr<Hooke<double> > hooke_double(new Hooke<double>());

  NonlinearElasticElement iso_elastic(m_field,1);
  for (_IT_CUBITMESHSETS_BY_BCDATA_TYPE_FOR_LOOP_(
           m_field, BLOCKSET | MAT_ELASTICSET, it)) {
    if (it->getName() != "MAT_ELASTIC_1")
      continue;
    Mat_Elastic mydata;
    CHKERR it->getAttributeDataStructure(mydata);
    int id = it->getMeshsetId();
    EntityHandle meshset = it->getMeshset();
    CHKERR moab.get_entities_by_type(meshset, MBTET,
                                     iso_elastic.setOfBlocks[id].tEts, true);
    iso_elastic.setOfBlocks[id].iD = id;
    iso_elastic.setOfBlocks[id].E = mydata.data.Young;
    iso_elastic.setOfBlocks[id].PoissonRatio = mydata.data.Poisson;
    iso_elastic.setOfBlocks[id].materialDoublePtr = hooke_double;
    iso_elastic.setOfBlocks[id].materialAdoublePtr = hooke_adouble;
    CHKERR m_field.seed_finite_elements(iso_elastic.setOfBlocks[id].tEts);
  }

  CHKERR iso_elastic.addElement("ELASTIC", "DISPLACEMENT");
  CHKERR iso_elastic.setOperators("DISPLACEMENT", "MESH_NODE_POSITIONS", false,
                                  true);
  if (m_field.check_field("POTENTIAL_FIELD")) {
    cout <<"Potential POTENTIAL_FIELD field exists" << endl; 
    CHKERR m_field.modify_finite_element_add_field_data("ELASTIC", "POTENTIAL_FIELD");
  }

  NonlinearElasticElement trans_elastic(m_field,2);
  trans_elastic.commonData.spatialPositions = "DISPLACEMENT";
  trans_elastic.commonData.meshPositions = "MESH_NODE_POSITIONS";
  std::map<int, boost::shared_ptr<SmallStrainTranverslyIsotropicADouble>>
      tranversly_isotropic_adouble_ptr_map;
  std::map<int, boost::shared_ptr<SmallStrainTranverslyIsotropicDouble>>
      tranversly_isotropic_double_ptr_map;
  bool trans_iso_blocks = false;
  for(_IT_CUBITMESHSETS_BY_SET_TYPE_FOR_LOOP_(m_field,BLOCKSET,it)) {
    //Get block name
    string name = it->getName();
    if (name.compare(0,20,"MAT_ELASTIC_TRANSISO") == 0) {
      trans_iso_blocks = true;
      int id = it->getMeshsetId();
      Mat_Elastic_TransIso mydata;
      CHKERR it->getAttributeDataStructure(mydata);
      tranversly_isotropic_adouble_ptr_map[id] =
          boost::make_shared<SmallStrainTranverslyIsotropicADouble>();
      tranversly_isotropic_double_ptr_map[id] =
          boost::make_shared<SmallStrainTranverslyIsotropicDouble>();
      //nu_p, nu_pz, E_p, E_z, G_zp
      tranversly_isotropic_adouble_ptr_map.at(id)->E_p = mydata.data.Youngp;
      tranversly_isotropic_double_ptr_map.at(id)->E_p = mydata.data.Youngp;
      tranversly_isotropic_adouble_ptr_map.at(id)->E_z = mydata.data.Youngz;
      tranversly_isotropic_double_ptr_map.at(id)->E_z = mydata.data.Youngz;
      tranversly_isotropic_adouble_ptr_map.at(id)->nu_p = mydata.data.Poissonp;
      tranversly_isotropic_double_ptr_map.at(id)->nu_p = mydata.data.Poissonp;
      tranversly_isotropic_adouble_ptr_map.at(id)->nu_pz = mydata.data.Poissonpz;
      tranversly_isotropic_double_ptr_map.at(id)->nu_pz = mydata.data.Poissonpz;
      double shear_zp;
      if(mydata.data.Shearzp!=0) {
        shear_zp = mydata.data.Shearzp;
      } else {
        shear_zp = mydata.data.Youngz/(2*(1+mydata.data.Poissonpz));
      }
      tranversly_isotropic_adouble_ptr_map.at(it->getMeshsetId())->G_zp = shear_zp;
      tranversly_isotropic_double_ptr_map.at(it->getMeshsetId())->G_zp = shear_zp;
      //get tets from block where material is defined
      EntityHandle meshset = it->getMeshset();
      CHKERR m_field.get_moab().get_entities_by_type(
          meshset, MBTET, trans_elastic.setOfBlocks[id].tEts, true);
      //adding material to nonlinear class
      trans_elastic.setOfBlocks[id].iD = id;
      //note that material parameters are defined internally in material model
      trans_elastic.setOfBlocks[id].E = 0; // this is not working for this material
      trans_elastic.setOfBlocks[id].PoissonRatio = 0; // this is not working for this material
      trans_elastic.setOfBlocks[id].materialDoublePtr =
          tranversly_isotropic_double_ptr_map.at(id);
      trans_elastic.setOfBlocks[id].materialAdoublePtr =
          tranversly_isotropic_adouble_ptr_map.at(id);
      CHKERR m_field.seed_finite_elements(trans_elastic.setOfBlocks[id].tEts);
    }
  }

  if(trans_iso_blocks) {
    CHKERR m_field.add_finite_element("TRAN_ISOTROPIC_ELASTIC");
    CHKERR m_field.add_finite_element("TRAN_ISOTROPIC_ELASTIC", MF_ZERO);
    CHKERR m_field.modify_finite_element_add_field_row("TRAN_ISOTROPIC_ELASTIC",
                                                       "DISPLACEMENT");
    CHKERR m_field.modify_finite_element_add_field_col("TRAN_ISOTROPIC_ELASTIC",
                                                       "DISPLACEMENT");
    CHKERR m_field.modify_finite_element_add_field_data(
        "TRAN_ISOTROPIC_ELASTIC", "DISPLACEMENT");
    CHKERR m_field.modify_finite_element_add_field_data(
        "TRAN_ISOTROPIC_ELASTIC", "POTENTIAL_FIELD");
    if(m_field.check_field("MESH_NODE_POSITIONS")) {
      CHKERR m_field.modify_finite_element_add_field_data(
          "TRAN_ISOTROPIC_ELASTIC", "MESH_NODE_POSITIONS");
    }
    for (map<int, NonlinearElasticElement::BlockData>::iterator sit =
             trans_elastic.setOfBlocks.begin();
         sit != trans_elastic.setOfBlocks.end(); sit++) {
      CHKERR m_field.add_ents_to_finite_element_by_type(
          sit->second.tEts, MBTET, "TRAN_ISOTROPIC_ELASTIC");
      CHKERRQ(ierr);
    }
  }

  if(trans_iso_blocks) {
    //Rhs
    trans_elastic.feRhs.getOpPtrVector().push_back(
        new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
            "DISPLACEMENT", trans_elastic.commonData));
    trans_elastic.feRhs.getOpPtrVector().push_back(
        new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
            "POTENTIAL_FIELD", trans_elastic.commonData));
    if(m_field.check_field("MESH_NODE_POSITIONS")) {
      trans_elastic.feRhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
              "MESH_NODE_POSITIONS", trans_elastic.commonData));
    }
    map<int, NonlinearElasticElement::BlockData>::iterator sit =
        trans_elastic.setOfBlocks.begin();
    for(;sit!=trans_elastic.setOfBlocks.end();sit++) {
      trans_elastic.feRhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpJacobianPiolaKirchhoffStress(
              "DISPLACEMENT", sit->second, trans_elastic.commonData, 2, false,
              false, true));
      trans_elastic.feRhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpRhsPiolaKirchhoff(
              "DISPLACEMENT", sit->second, trans_elastic.commonData));
    }

    //Lhs
    trans_elastic.feLhs.getOpPtrVector().push_back(
        new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
            "DISPLACEMENT", trans_elastic.commonData));
    trans_elastic.feLhs.getOpPtrVector().push_back(
        new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
            "POTENTIAL_FIELD", trans_elastic.commonData));
    if(m_field.check_field("MESH_NODE_POSITIONS")) {
      trans_elastic.feLhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpGetCommonDataAtGaussPts(
              "MESH_NODE_POSITIONS", trans_elastic.commonData));
    }
    sit = trans_elastic.setOfBlocks.begin();
    for(;sit!=trans_elastic.setOfBlocks.end();sit++) {
      trans_elastic.feLhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpJacobianPiolaKirchhoffStress(
              "DISPLACEMENT", sit->second, trans_elastic.commonData, 2, true,
              false, true));
      trans_elastic.feLhs.getOpPtrVector().push_back(
          new NonlinearElasticElement::OpLhsPiolaKirchhoff_dx(
              "DISPLACEMENT", "DISPLACEMENT", sit->second,
              trans_elastic.commonData));
    }
  }

  BCs_RVELagrange_Disp lagrangian_element_disp(m_field);
  lagrangian_element_disp.addLagrangiangElement(
      "LAGRANGE_ELEM", "DISPLACEMENT", "LAGRANGE_MUL_DISP",
      "MESH_NODE_POSITIONS");

  //build finite elements
  CHKERR m_field.build_finite_elements();
  //build adjacencies
  CHKERR m_field.build_adjacencies(bit_level0);

  //define problems
  CHKERR m_field.add_problem("ELASTIC_MECHANICS");
  //set finite elements for problem
  CHKERR m_field.modify_problem_add_finite_element("ELASTIC_MECHANICS",
                                                   "ELASTIC");
  if(trans_iso_blocks) {
    CHKERR m_field.modify_problem_add_finite_element("ELASTIC_MECHANICS",
                                                     "TRAN_ISOTROPIC_ELASTIC");
  }
  CHKERR m_field.modify_problem_add_finite_element("ELASTIC_MECHANICS",
                                                   "LAGRANGE_ELEM");


  //set refinement level for problem
  CHKERR m_field.modify_problem_ref_level_add_bit("ELASTIC_MECHANICS",
                                                  bit_level0);
  //build problem
  CHKERR m_field.build_problems();

  /****/
  //mesh partitioning

  //partition
  CHKERR m_field.partition_problem("ELASTIC_MECHANICS");
  CHKERR m_field.partition_finite_elements(
      "ELASTIC_MECHANICS", false, 0,
      m_field.get_comm_size() // build elements on all procs
  );
  //what are ghost nodes, see Petsc Manual
  CHKERR m_field.partition_ghost_dofs("ELASTIC_MECHANICS");

  //create matrices
  Vec D;
  vector<Vec> F(6);
  CHKERR m_field.VecCreateGhost("ELASTIC_MECHANICS",ROW,&F[0]); CHKERRQ(ierr);
  for(int ii = 1;ii<6;ii++) {
    CHKERR VecDuplicate(F[0], &F[ii]);
  }
  CHKERR m_field.VecCreateGhost("ELASTIC_MECHANICS", COL, &D);

  Mat Aij;
  CHKERR m_field.MatCreateMPIAIJWithArrays("ELASTIC_MECHANICS", &Aij);
  CHKERR MatSetOption(Aij, MAT_STRUCTURALLY_SYMMETRIC, PETSC_TRUE);
  CHKERR MatSetOption(Aij, MAT_NEW_NONZERO_LOCATIONS, PETSC_TRUE);
  CHKERR MatSetOption(Aij, MAT_KEEP_NONZERO_PATTERN, PETSC_TRUE);
  CHKERR MatSetOption(Aij, MAT_USE_INODES, PETSC_TRUE);
  CHKERR MatSetOption(Aij, MAT_NEW_NONZERO_LOCATION_ERR, PETSC_FALSE);
  CHKERR MatSetOption(Aij, MAT_KEEP_NONZERO_PATTERN, PETSC_FALSE);

  /*{
    ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
    std::string wait;
    std::cin >> wait;
  }*/

  CHKERR VecZeroEntries(D);
  CHKERR VecGhostUpdateBegin(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR VecGhostUpdateEnd(D, INSERT_VALUES, SCATTER_FORWARD);
  CHKERR m_field.set_global_ghost_vector(
    "ELASTIC_MECHANICS",ROW,D,INSERT_VALUES,SCATTER_REVERSE
  ); CHKERRQ(ierr);
  for(int ii = 0;ii<6;ii++) {
    CHKERR VecZeroEntries(F[ii]);
    CHKERR VecGhostUpdateBegin(F[ii], INSERT_VALUES, SCATTER_FORWARD);
    CHKERR VecGhostUpdateEnd(F[ii], INSERT_VALUES, SCATTER_FORWARD);
  }
  CHKERR MatZeroEntries(Aij);

  Vec volume_vec;
  int volume_vec_ghost[] = { 0 };
  ierr = VecCreateGhost(
    PETSC_COMM_WORLD,(!m_field.get_comm_rank())?1:0,1,1,volume_vec_ghost,&volume_vec
  );  CHKERRQ(ierr);
  ierr = VecZeroEntries(volume_vec); CHKERRQ(ierr);

  iso_elastic.getLoopFeLhs().getOpPtrVector().push_back(
      new VolumeCalculation("DISPLACEMENT", volume_vec));
  trans_elastic.getLoopFeLhs().getOpPtrVector().push_back(
      new VolumeCalculation("DISPLACEMENT", volume_vec));

  //iso_elastic element matrix
  iso_elastic.getLoopFeLhs().snes_x = D;
  iso_elastic.getLoopFeLhs().snes_B = Aij;
  trans_elastic.getLoopFeLhs().snes_x = D;
  trans_elastic.getLoopFeLhs().snes_B = Aij;
  CHKERR m_field.loop_finite_elements("ELASTIC_MECHANICS", "ELASTIC",
                                      iso_elastic.getLoopFeLhs());
  if(m_field.check_finite_element("TRAN_ISOTROPIC_ELASTIC"))
    CHKERR m_field.loop_finite_elements("ELASTIC_MECHANICS",
                                        "TRAN_ISOTROPIC_ELASTIC",
                                        trans_elastic.getLoopFeLhs());

  lagrangian_element_disp.setRVEBCsOperators(
      "DISPLACEMENT", "LAGRANGE_MUL_DISP", "MESH_NODE_POSITIONS", Aij, F);
  CHKERR m_field.loop_finite_elements(
      "ELASTIC_MECHANICS", "LAGRANGE_ELEM",
      lagrangian_element_disp.getLoopFeRVEBCLhs());
  CHKERR m_field.loop_finite_elements(
      "ELASTIC_MECHANICS", "LAGRANGE_ELEM",
      lagrangian_element_disp.getLoopFeRVEBCRhs());

  for(int ii = 0;ii<6;ii++) {
    ierr = VecGhostUpdateBegin(F[ii],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(F[ii],ADD_VALUES,SCATTER_REVERSE); CHKERRQ(ierr);
    ierr = VecAssemblyBegin(F[ii]); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(F[ii]); CHKERRQ(ierr);
  }
  ierr = MatAssemblyBegin(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);
  ierr = MatAssemblyEnd(Aij,MAT_FINAL_ASSEMBLY); CHKERRQ(ierr);

  // ierr = MatView(Aij,PETSC_VIEWER_DRAW_SELF); CHKERRQ(ierr);
  // std::string wait;
  // std::cin >> wait;


  ierr = VecAssemblyBegin(volume_vec); CHKERRQ(ierr);
  ierr = VecAssemblyEnd(volume_vec); CHKERRQ(ierr);
  double rve_volume;
  ierr = VecSum(volume_vec,&rve_volume);  CHKERRQ(ierr);
  ierr = PetscPrintf(PETSC_COMM_WORLD,"RVE Volume %3.2g\n",rve_volume); CHKERRQ(ierr);

  ierr = VecDestroy(&volume_vec);

  // Solver
  KSP solver;
  ierr = KSPCreate(PETSC_COMM_WORLD,&solver); CHKERRQ(ierr);
  ierr = KSPSetOperators(solver,Aij,Aij); CHKERRQ(ierr);
  ierr = KSPSetFromOptions(solver); CHKERRQ(ierr);
  ierr = KSPSetUp(solver); CHKERRQ(ierr);

  MatrixDouble Dmat;
  Dmat.resize(6,6);
  Dmat.clear();

  //create a vector for 6 components of homogenized stress
  Vec stress_homo;
  int stress_homo_ghost[] = { 0,1,2,3,4,5,6 };
  NonlinearElasticElement::MyVolumeFE ave_stress_iso(m_field);
  NonlinearElasticElement::MyVolumeFE ave_stress_trans(m_field);
  PetscBool stress_by_boundary_integral = PETSC_FALSE;
  ierr = VecCreateGhost(
    PETSC_COMM_WORLD,(!m_field.get_comm_rank())?6:0,6,6,stress_homo_ghost,&stress_homo
  );  CHKERRQ(ierr);

  lagrangian_element_disp.setRVEBCsHomoStressOperators(
    "DISPLACEMENT","LAGRANGE_MUL_DISP","MESH_NODE_POSITIONS",stress_homo
  );
 

  struct MyPostProc: public PostProcVolumeOnRefinedMesh {

    bool doPreProcess;
    bool doPostProcess;

    MyPostProc(MoFEM::Interface &m_field):
    PostProcVolumeOnRefinedMesh(m_field),
    doPreProcess(true),
    doPostProcess(true)
    {}

    void setDoPreProcess() { doPreProcess = true; }
    void unSetDoPreProcess() { doPreProcess = false; }
    void setDoPostProcess() { doPostProcess = true; }
    void unSetDoPostProcess() { doPostProcess = false; }

    PetscErrorCode preProcess() {
      PetscFunctionBegin;
      if(doPreProcess) {
        ierr = PostProcVolumeOnRefinedMesh::preProcess(); CHKERRQ(ierr);
      }
      PetscFunctionReturn(0);
    }
    PetscErrorCode postProcess() {
      PetscFunctionBegin;
      if(doPostProcess) {
        ierr = PostProcVolumeOnRefinedMesh::postProcess(); CHKERRQ(ierr);
      }
      PetscFunctionReturn(0);
    }
  };

  MyPostProc post_proc(m_field);
  ierr = post_proc.generateReferenceElementMesh(); CHKERRQ(ierr);
  ierr = post_proc.addFieldValuesPostProc("DISPLACEMENT"); CHKERRQ(ierr);
  ierr = post_proc.addFieldValuesGradientPostProc("DISPLACEMENT"); CHKERRQ(ierr);
  ierr = post_proc.addFieldValuesPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
  ierr = post_proc.addFieldValuesGradientPostProc("MESH_NODE_POSITIONS"); CHKERRQ(ierr);
  if(trans_iso_blocks) {
    ierr = post_proc.addFieldValuesGradientPostProc("POTENTIAL_FIELD"); CHKERRQ(ierr);
  }
  for(
    map<int,NonlinearElasticElement::BlockData>::iterator sit = iso_elastic.setOfBlocks.begin();
    sit != iso_elastic.setOfBlocks.end(); sit++
  ) {
    post_proc.getOpPtrVector().push_back(
      new PostPorcStress(
        post_proc.postProcMesh,
        post_proc.mapGaussPts,
        "DISPLACEMENT",
        sit->second,
        post_proc.commonData,
        false
      )
    );
  }
  for(
    map<int,NonlinearElasticElement::BlockData>::iterator sit = trans_elastic.setOfBlocks.begin();
    sit != trans_elastic.setOfBlocks.end(); sit++
  ) {
    post_proc.getOpPtrVector().push_back(
      new PostPorcStress(
        post_proc.postProcMesh,
        post_proc.mapGaussPts,
        "DISPLACEMENT",
        sit->second,
        post_proc.commonData
      )
    );
  }

  PetscScalar *avec;
  ierr = VecGetArray(stress_homo,&avec); CHKERRQ(ierr);
  for(int ii = 0;ii<6;ii++) {
    ierr = VecZeroEntries(D); CHKERRQ(ierr);
    ierr = KSPSolve(solver,F[ii],D); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(D,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = m_field.set_global_ghost_vector(
      "ELASTIC_MECHANICS",ROW,D,INSERT_VALUES,SCATTER_REVERSE
    ); CHKERRQ(ierr);
    post_proc.setDoPreProcess();
    post_proc.unSetDoPostProcess();
    ierr = m_field.loop_finite_elements(
      "ELASTIC_MECHANICS","ELASTIC",post_proc
    ); CHKERRQ(ierr);
    post_proc.unSetDoPreProcess();
    post_proc.setDoPostProcess();

    if(m_field.check_finite_element("TRAN_ISOTROPIC_ELASTIC"))
      ierr = m_field.loop_finite_elements(
        "ELASTIC_MECHANICS","TRAN_ISOTROPIC_ELASTIC",post_proc
      ); CHKERRQ(ierr);
    {
      ostringstream sss;
      sss << "mode_disp_" <<  ii << ".h5m";
      rval = post_proc.postProcMesh.write_file(
        sss.str().c_str(),"MOAB","PARALLEL=WRITE_PART"
      ); CHKERRQ_MOAB(rval);
    }
    ierr = VecZeroEntries(stress_homo); CHKERRQ(ierr);
      ierr = m_field.loop_finite_elements(
        "ELASTIC_MECHANICS","LAGRANGE_ELEM",lagrangian_element_disp.getLoopFeRVEBCStress()
      ); CHKERRQ(ierr);
    ierr = PetscOptionsGetReal(
      PETSC_NULL,"-my_rve_volume",&rve_volume,PETSC_NULL
    ); CHKERRQ(ierr);
    ierr = VecAssemblyBegin(stress_homo); CHKERRQ(ierr);
    ierr = VecAssemblyEnd(stress_homo); CHKERRQ(ierr);
    ierr = VecScale(stress_homo,1.0/rve_volume); CHKERRQ(ierr);
    ierr = VecGhostUpdateBegin(stress_homo,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    ierr = VecGhostUpdateEnd(stress_homo,INSERT_VALUES,SCATTER_FORWARD); CHKERRQ(ierr);
    for(int jj=0; jj<6; jj++) {
      Dmat(jj,ii) = avec[jj];
    }
  }
  ierr = VecRestoreArray(stress_homo,&avec); CHKERRQ(ierr);

  PetscPrintf(
    PETSC_COMM_WORLD,"\nHomogenised Stiffens Matrix = \n\n"
  );

  for(int ii=0; ii<6; ii++) {
    PetscPrintf(
      PETSC_COMM_WORLD,
      "stress %d\t\t%8.5e\t\t%8.5e\t\t%8.5e\t\t%8.5e\t\t%8.5e\t\t%8.5e\n",
      ii,Dmat(ii,0),Dmat(ii,1),Dmat(ii,2),Dmat(ii,3),Dmat(ii,4),Dmat(ii,5)
    );
  }

  // //Saving Dmat as a bindary file to use it macro-structure


  char output_file_Dmat[255];
  ierr = PetscOptionsGetString(PETSC_NULL,"-my_output_file_Dmat",output_file_Dmat,255,&flg); CHKERRQ(ierr);
  if(flg) {

    //Reading and writing binary files
    if(pcomm->rank()==0){
      int fd;
      PetscViewer view_out;
      PetscViewerBinaryOpen(PETSC_COMM_SELF,output_file_Dmat,FILE_MODE_WRITE,&view_out);
      PetscViewerBinaryGetDescriptor(view_out,&fd);
      PetscBinaryWrite(fd,&Dmat(0,0),36,PETSC_DOUBLE,PETSC_FALSE);
      PetscViewerDestroy(&view_out);
    }
  }

  //detroy matrices
  for(int ii = 0;ii<6;ii++) {
    ierr = VecDestroy(&F[ii]); CHKERRQ(ierr);
  }
  ierr = VecDestroy(&D); CHKERRQ(ierr);
  ierr = MatDestroy(&Aij); CHKERRQ(ierr);
  ierr = KSPDestroy(&solver); CHKERRQ(ierr);
  ierr = VecDestroy(&stress_homo); CHKERRQ(ierr);

  }
  CATCH_ERRORS;


  MoFEM::Core::Finalize();
}
